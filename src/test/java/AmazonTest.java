import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pageObjects.HeaderComponent;
import pageObjects.SearchResultsPage;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@Listeners({ScreenshotListener.class})
public class AmazonTest {
    private WebDriver driver;

    private HeaderComponent headerComponent;
    private SearchResultsPage searchResultsPage;

    public WebDriver getDriver() {
        return driver;
    }

    @BeforeTest
    public void setup() {
        WebDriverManager.firefoxdriver().setup();
        driver = new FirefoxDriver();
        driver.get("https://www.amazon.com");

        headerComponent = new HeaderComponent(driver);
        searchResultsPage = new SearchResultsPage(driver);
    }

    @AfterTest
    public void cleanUp() {
        driver.close();
    }

    @Test
    public void simpleSearchTest() {
        headerComponent.search("Selenium WebDriver");

        // check search result list
        assertThat(searchResultsPage.getResultsText(), containsString("results for \"Selenium WebDriver\""));

        List<WebElement> searchResults = searchResultsPage.getResultList();

        for(WebElement item : searchResults) {
            assertThat(item.getText().toLowerCase(), containsString("selenium"));
        }
    }

    @Test
    public void addToCartTest() {
        headerComponent.search("test");

        // open first search result
        driver.findElement(By.cssSelector("[data-component-type=\"s-search-results\"] h5")).click();

        // store product name for later verification
        String productTitle = driver.findElement(By.id("productTitle")).getText();

        // add to cart
        driver.findElement(By.id("add-to-cart-button")).click();

        // view cart
        driver.findElement(By.id("hlb-view-cart-announce")).click();

        // verify cart item title equals to product name
        assertThat(driver.findElement(By.className("sc-product-title")).getText(),
                containsString(productTitle));
    }
}
