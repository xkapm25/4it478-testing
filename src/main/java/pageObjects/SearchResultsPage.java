package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class SearchResultsPage {
    private WebDriver driver;

    @FindBy(css = "[data-component-type=\"s-result-info-bar\"]")
    private WebElement resultInfoBar;

    @FindBy(css = "\".s-result-list h5\"")
    private List<WebElement> resultList;

    public SearchResultsPage(WebDriver driver) {
        this.driver = driver;

        PageFactory.initElements(driver, this);
    }

    public String getResultsText() {
        return resultInfoBar.getText();
    }

    public List<WebElement> getResultList() {
        return resultList;
    }
}